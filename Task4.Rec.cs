using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task_4
{     
    class Point
    {
        /// <summary>
        /// coordinates of point
        /// </summary>
        public int X { get; set; }
        public int Y { get; set; }

        // defoult ctor
        public Point()
        {       
        }

        // ctor with parameters
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }

     class Rectangle
    {
       /// <summary>
        /// List of rectangle coordinates
       /// </summary>
        private List<Point> _points ;

        /// <summary>
        /// Properties to access the coordinates
        /// </summary>
        public List<Point> ListPoint
        {
            get
            {
                return _points;
            }
        }
        
        /// <summary>
        /// constructor without parameters
        /// </summary>
        public Rectangle(): this(new Point(), 0, 0)
        {            
        }

        /// <summary>
        /// constructor with parameters
        /// </summary>
        /// <param name="bottomLeft">statrt point of rectangle</param>
        /// <param name="longX">length of x</param>
        /// <param name="longY">length of x</param>
        public Rectangle(Point bottomLeft, int longX, int longY)
        {
            Point bottomRight = new Point
                {
                    X = bottomLeft.X + longX,
                    Y = bottomLeft.Y
                };

            Point topLeft = new Point
            {
                X = bottomLeft.X,
                Y = bottomLeft.Y + longY
            };

            Point topRight = new Point
                {
                    X = bottomRight.X,
                    Y = topLeft.Y
                };

            _points = new List<Point>
            {
                bottomLeft,
                bottomRight,
                topLeft,
                topRight
            };
           
        }
//Method to change the coordinates of the rectangle
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="startPointX"></param>
        /// <param name="startPointY"></param>
        /// <param name="longX"></param>
        /// <param name="longY"></param>
        public  void ChengeCoordinatesRectangle(int startPointX, int startPointY, int longX, int longY)
        {
           
            Point bottomLeft = new Point
                {
                    X = startPointX,
                    Y = startPointY
                };

            Point bottomRight = new Point
                {
                    X = startPointX + longX,
                    Y = startPointY
                };

            Point topLeft = new Point
                {
                    X = startPointX,
                    Y = startPointY + longY
                };

            Point topRight = new Point
                {
                    X = bottomLeft.X,
                    Y = topLeft.Y
                };

            _points = new List<Point>
            {
                bottomLeft,
                bottomRight,
                topLeft,
                topRight
            };
        }

        
        /// <summary>
        /// move rectangle to X
        /// </summary>
        /// <param name="moveX">offset distance to X</param>
        public void MoveToX(int moveX)
        {
            foreach(var i in _points)
            {
                i.X += moveX;
            }
        }

        /// <summary>
        /// move rectangle to Y
        /// </summary>
        /// <param name="moveY">offset distance to Y</param>
        public void MoveToY(int moveY)
        {
            foreach (var i in _points)
            {
                i.Y += moveY;
            }
        }

        /// <summary>
        /// move rectangle to X and Y
        /// </summary>
        /// <param name="moveX">offset distance to X</param>
        /// <param name="moveY">offset distance to Y</param>
        public void MoveToXandY(int moveX, int moveY)
        {
            foreach (var i in _points)
            {
                i.X += moveX;
                i.Y += moveY;
            }
        }
    }

     class Program
    {
        static void Main(string[] args)
        {
            // creating instance of type Rectangle
            Rectangle rectangle1 = new Rectangle();
            Rectangle rectangle2 = new Rectangle();

            // check
            rectangle1.MoveToX(5);

            Rectangle rect3 = new Rectangle();
            rect3 = GetMaxJointRectangle(rectangle1, rectangle2);
            


            Console.ReadKey();


        }

        /// <summary>
        /// method for finding joint rectangle
        /// </summary>
        /// <param name="first">first rectangle</param>
        /// <param name="second">second rectangle</param>
        /// <returns> new joint rectangle</returns>
        static Rectangle GetMaxJointRectangle(Rectangle first, Rectangle second)
        {
            // temporary variable
            int firstXmin, firstXmax, firstYmin, firstYmax;
            int secondXmin, secondXmax, secondYmin, secondYmax;

            var tmpX = first.ListPoint;

            var tmpY = second.ListPoint;

            firstXmin = tmpX.Select(p => p.X).Min();
            firstXmax = tmpX.Select(p => p.X).Max();

            firstYmin = tmpX.Select(p => p.Y).Min();
            firstYmax = tmpX.Select(p => p.Y).Max();

            secondXmin = (from p in tmpY select p.X).Min();
            secondXmax = (from p in tmpY select p.X).Max();

            secondYmin = (from p in tmpY select p.Y).Min();
            secondYmax = (from p in tmpY select p.Y).Max();

            int tempXmin, tempXmax, tempYmin, tempYmax;

            // coordinate search
            if (firstXmin < secondXmin)
            {
                tempXmin = firstXmin;
            }
            else
            {
                tempXmin = secondXmin;
            }

            if (firstYmin < secondYmin)
            {
                tempYmin = firstYmin;
            }
            else
            {
                tempYmin = secondYmin;
            }

            if (firstXmax < secondXmax)
            {
                tempXmax = firstXmax;
            }
            else
            {
                tempXmax = secondXmax;
            }

            if (firstYmax < secondYmax)
            {
                tempYmax = firstYmax;
            }
            else
            {
                tempYmax = secondYmax;
            }

            Point startMin = new Point(tempXmin, tempYmin);

            return new Rectangle(startMin, tempXmax - tempYmin, tempYmax - tempYmin);


        }

        /// <summary>
        /// this method search min lowest common rectangle
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        static Rectangle GetMinJointRectangle(Rectangle first, Rectangle second)
        {
            int firstXmin, firstXmax, firstYmin, firstYmax;
            int secondXmin, secondXmax, secondYmin, secondYmax;

            var tmpX = first.ListPoint;

            var tmpY = second.ListPoint;

            firstXmin = tmpX.Select(p => p.X).Min();
            firstXmax = tmpX.Select(p => p.X).Max();

            firstYmin = tmpX.Select(p => p.Y).Min();
            firstYmax = tmpX.Select(p => p.Y).Max();

            secondXmin = (from p in tmpY select p.X).Min();
            secondXmax = (from p in tmpY select p.X).Max();

            secondYmin = (from p in tmpY select p.Y).Min();
            secondYmax = (from p in tmpY select p.Y).Max();

            int tempXmin, tempXmax, tempYmin, tempYmax;

            //coordinate search
            if (firstXmin >= secondXmin)
            {
                tempXmin = firstXmin;
            }
            else
            {
                tempXmin = secondXmin;
            }

            if (firstYmin >= secondYmin)
            {
                tempYmin = firstYmin;
            }
            else
            {
                tempYmin = secondYmin;
            }

            if (firstXmax >= secondXmax)
            {
                tempXmax = firstXmax;
            }
            else
            {
                tempXmax = secondXmax;
            }

            if (firstYmax >= secondYmax)
            {
                tempYmax = firstYmax;
            }
            else
            {
                tempYmax = secondYmax;
            }

            Point startMin = new Point(tempXmin, tempYmin);

            return new Rectangle(startMin, tempXmax - tempYmin, tempYmax - tempYmin);

        }
    }
}